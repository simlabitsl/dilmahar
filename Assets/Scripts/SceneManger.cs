﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManger : MonoBehaviour {


	// Use this for initialization
	void Start () 
	{
		StartCoroutine (ChangeScene ());
	}
	
	IEnumerator ChangeScene()
	{
		yield return new WaitForSeconds (3f);
		SceneManager.LoadScene (1);
	}
}
